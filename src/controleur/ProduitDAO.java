package controleur;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import bddutils.EntityClass;
import modele.Produit;

public class ProduitDAO {

	

	public static boolean addProduit(Produit produit){
		
		EntityClass.em.getTransaction().begin();
		EntityClass.em.persist(produit);
		EntityClass.em.getTransaction().commit();
		
		if (EntityClass.em.contains(produit)) {
			  return true;
		} 
		return false;
	}	
	
	public static List<Produit> getAllProduits(){
		
			Query query = EntityClass.em.createQuery("from Produit"); 
			
			List<Produit> list = null;
			
			if(query.getResultList().stream().findFirst().orElse(null)==null)
				return null;
			else list = query.getResultList();
			
			return list;
	}
	
	public static Produit getProduit(int idProduit){
		
		String query = "from Produit WHERE idProduit = :ID";
		
		TypedQuery<Produit> tq = EntityClass.em.createQuery(query, Produit.class);
		tq.setParameter("ID", idProduit);
		Produit produit = null;
		
		if(tq.getResultList().stream().findFirst().orElse(null)==null)
			return null;
		else produit = tq.getSingleResult();
			
		return produit;
	}
	
	public static Produit getProduit(int idProduit,int idFournisseur){
		
		String query = "from Produit WHERE idProduit = :IDP and fournisseur_idFournisseur = :IDF";
		
		TypedQuery<Produit> tq = EntityClass.em.createQuery(query, Produit.class);
		tq.setParameter("IDP", idProduit);
		tq.setParameter("IDF", idFournisseur);
		Produit produit = null;
		
		if(tq.getResultList().stream().findFirst().orElse(null)==null)
			return null;
		else produit = tq.getSingleResult();
			
		return produit;
	}


    public static boolean ModifProduit(Produit newProduit){
	
	    EntityClass.em.getTransaction().begin();
		Produit oldProduit = EntityClass.em.find(Produit.class, newProduit.getIdProduit());
		
		if(oldProduit != null)
		{
			oldProduit.setAchat(newProduit.getAchat());
			oldProduit.setCategorie(newProduit.getCategorie());
			oldProduit.setCommande(newProduit.getCommande());
			oldProduit.setEspece(newProduit.getEspece());
			oldProduit.setFournisseur(newProduit.getFournisseur());
			oldProduit.setNomProduit(newProduit.getNomProduit());
			oldProduit.setPrixU(newProduit.getPrixU());
			oldProduit.setQteDispo(newProduit.getQteDispo());
			
			EntityClass.em.persist(oldProduit);
			EntityClass.em.getTransaction().commit();
			return true;
		}
		return false;
    }
    
    public static boolean UpdateQteProduit(int idProduit, int QtePrise){
    	
	    EntityClass.em.getTransaction().begin();
		Produit oldProduit = EntityClass.em.find(Produit.class, idProduit);
		
		if(oldProduit != null)
		{
			int QteDispo = oldProduit.getQteDispo();
			oldProduit.setQteDispo(QteDispo - QtePrise);
			
			EntityClass.em.persist(oldProduit);
			EntityClass.em.getTransaction().commit();
			return true;
		}
		return false;
    }
    
 public static boolean AjoutStockProduit(int idProduit, int QteAjoutee){
    	
	    EntityClass.em.getTransaction().begin();
		Produit oldProduit = EntityClass.em.find(Produit.class, idProduit);
		
		if(oldProduit != null)
		{
			int QteDispo = oldProduit.getQteDispo();
			oldProduit.setQteDispo(QteDispo + QteAjoutee);
			
			EntityClass.em.persist(oldProduit);
			EntityClass.em.getTransaction().commit();
			return true;
		}
		return false;
    }
    
    public static boolean DeleteProduit(int idProduit){
    	
    	EntityClass.em.getTransaction().begin();
		Produit produit = EntityClass.em.find(Produit.class, idProduit);

		if(produit != null)
		{
    		EntityClass.em.remove(produit);
    		EntityClass.em.persist(produit);
    		return true;
    	}
    	return false;
    }
    
    
    
}
