package controleur;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import bddutils.EntityClass;
import modele.Client;
import modele.Fournisseur;


public class FournisseurDAO { 

	

	public static boolean addFournisseur(Fournisseur fournisseur){
		
		EntityClass.em.getTransaction().begin();
		EntityClass.em.persist(fournisseur);
		EntityClass.em.getTransaction().commit();
		
		if (EntityClass.em.contains(fournisseur)) {
			  return true;
		} 
		return false;
	}	
	
	public static List<Fournisseur> getAllFournisseurs(){
		
			Query query = EntityClass.em.createQuery("from Fournisseur"); 
		
            List<Fournisseur> list = null;
			
			if(query.getResultList().stream().findFirst().orElse(null)==null)
				return null;
			else list = query.getResultList();
			
			return list;
	}
	
	public static Fournisseur getFournisseur(int idFournisseur){
		
		String query = "from Fournisseur WHERE idFournisseur = :ID";
		
		TypedQuery<Fournisseur> tq = EntityClass.em.createQuery(query, Fournisseur.class);
		tq.setParameter("ID", idFournisseur);
		Fournisseur fournisseur = null;
		
		if(tq.getResultList().stream().findFirst().orElse(null)==null)
			return null;
		else fournisseur = tq.getSingleResult();
		
		return fournisseur;
	}


    public static boolean ModifFournisseur(Fournisseur newFournisseur){
	
	    EntityClass.em.getTransaction().begin();
		Fournisseur oldFournisseur = EntityClass.em.find(Fournisseur.class, newFournisseur.getIdFournisseur());
		
		if(oldFournisseur != null)
		{
			oldFournisseur.setAdrFournisseur(newFournisseur.getAdrFournisseur());
			oldFournisseur.setNomFournisseur(newFournisseur.getNomFournisseur());
			oldFournisseur.setPnomFournisseur(newFournisseur.getPnomFournisseur());
			oldFournisseur.setProduits(newFournisseur.getProduits());
			oldFournisseur.setVilleFournisseur(newFournisseur.getVilleFournisseur());
			
			EntityClass.em.persist(oldFournisseur);
			EntityClass.em.getTransaction().commit();
			return true;
		}
		return false;
    }
    
    public static boolean DeleteFournisseur(int idFournisseur){
    	
    	EntityClass.em.getTransaction().begin();
		Fournisseur fournisseur = EntityClass.em.find(Fournisseur.class, idFournisseur);

		if(fournisseur != null)
		{
    		EntityClass.em.remove(fournisseur);
    		EntityClass.em.persist(fournisseur);
    		return true;
    	}
    	return false;
    }
}
