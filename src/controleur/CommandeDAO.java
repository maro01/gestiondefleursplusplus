package controleur;

import java.util.HashMap;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import bddutils.EntityClass;
import modele.Client;
import modele.Commande;


public class CommandeDAO {

	

	public static boolean addCommande(Commande commande){
		
		EntityClass.em.getTransaction().begin();
		EntityClass.em.persist(commande);
		EntityClass.em.getTransaction().commit();
		
		if (EntityClass.em.contains(commande)) {
			  return true;
		} 
		return false;
	}	
	
	public static List<Commande> getAllCommandes(){
		
			Query query = EntityClass.em.createQuery("from Commande,commande_listquantitesdemand"); 
		
            List<Commande> list = null;
			
			if(query.getResultList().stream().findFirst().orElse(null)==null)
				return null;
			else list = query.getResultList();
			
			return list;
	}
	
	public static Commande getCommande(int idCommande){
		
		String query = "from Commande a,commande_listquantitesdemand b WHERE a.idCommande = :ID";
		
		TypedQuery<Commande> tq = EntityClass.em.createQuery(query, Commande.class);
		tq.setParameter("ID", idCommande);
		Commande commande = null;
		
		if(tq.getResultList().stream().findFirst().orElse(null)==null)
			return null;
		else commande = tq.getSingleResult();
		
		return commande;
	}


    public static boolean ModifCommande(Commande newCommande){
	
	    EntityClass.em.getTransaction().begin();
		Commande oldCommande = EntityClass.em.find(Commande.class, newCommande.getIdCommande());
		
		if(oldCommande != null)
		{
			oldCommande.setAdmin(newCommande.getAdmin());
			oldCommande.setListProduits(newCommande.getListProduits());
			oldCommande.setListQuantitesDemand(newCommande.getListQuantitesDemand());

			EntityClass.em.persist(oldCommande);
			EntityClass.em.getTransaction().commit();
			return true;
		}
		return false;
    }
    
    public static boolean DeleteCommande(int idCommande){
    	
    	EntityClass.em.getTransaction().begin();
		Commande commande = EntityClass.em.find(Commande.class, idCommande);

		if(commande != null)
		{
    		EntityClass.em.remove(commande);
    		EntityClass.em.persist(commande);
    		return true;
    	}
    	return false;
    }
}
