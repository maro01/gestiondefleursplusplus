package controleur;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.persistence.TypedQuery;

import bddutils.EntityClass;
import modele.Achat;
import modele.Fournisseur;


public class AchatDAO {
	

	public static boolean addAchat(Achat achat){
		
		EntityClass.em.getTransaction().begin();
		EntityClass.em.persist(achat);
		EntityClass.em.getTransaction().commit();
		
		if (EntityClass.em.contains(achat)) {
			  return true;
		} 
		return false;
	}	
	
	public static List<Achat> getAllAchats(){
		
			Query query = EntityClass.em.createQuery("from Achat"); 
		
            List<Achat> list = null;
			
			if(query.getResultList().stream().findFirst().orElse(null)==null)
				return null;
			else list = query.getResultList();
			
			return list;
	}
	
	public static Achat getAchat(int idAchat){
		
		String query = "from Achat WHERE idAchat = :ID";
		
		TypedQuery<Achat> tq = EntityClass.em.createQuery(query, Achat.class);
		tq.setParameter("ID", idAchat);
		Achat achat = null;
		
		if(tq.getResultList().stream().findFirst().orElse(null)==null)
			return null;
		else achat = tq.getSingleResult();
		
		return achat;
	}


    public static boolean ModifAchat(Achat newAchat){
	
	    EntityClass.em.getTransaction().begin();
		Achat oldAchat = EntityClass.em.find(Achat.class, newAchat.getIdAchat());
		
		if(oldAchat != null)
		{
			oldAchat.setClient(newAchat.getClient());
			oldAchat.setListeQuantitesDemand(newAchat.getListeQuantitesDemand());
			oldAchat.setMontantTotal(newAchat.getMontantTotal());
			oldAchat.setPanier(newAchat.getPanier());
			
			EntityClass.em.persist(oldAchat);
			EntityClass.em.getTransaction().commit();
			return true;
		}
		return false;
    }
    
    public static boolean DeleteAchat(int idAchat){
    	
    	EntityClass.em.getTransaction().begin();
		Achat achat = EntityClass.em.find(Achat.class, idAchat);

		if(achat != null)
		{
    		EntityClass.em.remove(achat);
    		EntityClass.em.persist(achat);
    		return true;
    	}
    	return false;
    }
    
}
