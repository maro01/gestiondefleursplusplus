package controleur;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import bddutils.EntityClass;
import modele.Administrateur;
import modele.Produit;


public class AdministrateurDAO {
	

	public static boolean addAdministrateur(Administrateur Administrateur){
		
		EntityClass.em.getTransaction().begin();
		EntityClass.em.persist(Administrateur);
		EntityClass.em.getTransaction().commit();
		
		if (EntityClass.em.contains(Administrateur)) {
			  return true;
		} 
		return false;
	}	
}
