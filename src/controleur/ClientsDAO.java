package controleur;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import bddutils.EntityClass;
import modele.Achat;
import modele.Client;
import modele.Produit;


public class ClientsDAO {
	

	public static boolean addClient(Client client){
		
		EntityClass.em.getTransaction().begin();
		EntityClass.em.persist(client);
		EntityClass.em.getTransaction().commit();
		
		if (EntityClass.em.contains(client)) {
			  return true;
		} 
		return false;
	}	
	
	public static List<Client> getAllClients(){
		
			Query query = EntityClass.em.createQuery("from Utilisateur where TYPE='CL' "); 
								
            List<Client> list = null;
			
			if(query.getResultList().stream().findFirst().orElse(null)==null)
				return null;
			else list = query.getResultList();
			
			return list;
			
	}
	
	public static Client getClient(int idClient){
		
		String query = "from Utilisateur WHERE idUtilisateur = :ID AND TYPE='CL' ";
		
		TypedQuery<Client> tq = EntityClass.em.createQuery(query, Client.class);
		tq.setParameter("ID", idClient);
		Client client = null;
		
		if(tq.getResultList().stream().findFirst().orElse(null)==null)
			return null;
		else client = tq.getSingleResult();
		
		return client;
		
	}


    public static boolean ModifClient(Client newClient){
	
	    EntityClass.em.getTransaction().begin();
		Client oldClient = EntityClass.em.find(Client.class, newClient.getIdUtilisateur());
		
		if(oldClient != null)
		{
			oldClient.setAdrUtilisateur(newClient.getAdrUtilisateur());
			oldClient.setListeAchats(newClient.getListeAchats());
			oldClient.setNomUtilisateur(newClient.getNomUtilisateur());
			oldClient.setPnomUtilisateur(newClient.getPnomUtilisateur());
			oldClient.setReductionEligible(newClient.isReductionEligible());
			oldClient.setTauxReduction(newClient.getTauxReduction());
			oldClient.setVilleUtilisateur(newClient.getVilleUtilisateur());
			
			EntityClass.em.persist(oldClient);
			EntityClass.em.getTransaction().commit();
			return true;
		}
		return false;
    }
    
    public static boolean DeleteClient(int idClient){
    	
    	EntityClass.em.getTransaction().begin();
		Client client = EntityClass.em.find(Client.class, idClient);

		if(client != null)
		{
    		EntityClass.em.remove(client);
    		EntityClass.em.persist(client);
    		return true;
    	}
    	return false;
    }
}
