package view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.persistence.*;

import modele.Achat;
import controleur.ClientsDAO;
import controleur.FournisseurDAO;
import controleur.ProduitDAO;
import modele.Client;
import modele.Fournisseur;
import modele.Produit;
import bddutils.EntityClass;
import controleur.FournisseurDAO;

public class EspaceClient {
	public static void main(String[] args) {
		
		
		int nbrAchatsClient = 0;
		int nbrTransactionClient = -1;
		
		EntityClass.init();

		ProduitDAO produitController = new ProduitDAO();
		ClientsDAO clientController = new ClientsDAO();

	
	    Scanner sc = new Scanner(System.in);
		String choix="o";
		
		Client utilisateur = new Client();
	
		Achat achat;

		boolean repeat,quit,commande,acheter,otherpanier;
		do
		{
			achat = new Achat();
			achat.setClient(utilisateur);
			utilisateur.AjouterPanier(achat);
			nbrTransactionClient++;
		do
		{
			repeat=false; //boolean faux si on ne veux plus ajouter de produits
			quit=false; // boolean faux tant que ne quitte pas l'application
			commande=false; // boolean faux si on n'a pas encore remplit la liste des produits et leurs quantites
			acheter=false; //boolean faux si la transaction n'est pas realisee
			otherpanier=false; // boolean faux tant qu'on entre pas un choix si on veut un autre panier ou non
			
			
			List<Produit> listProduits = ProduitDAO.getAllProduits();
			System.out.println("-----------------------------------------------------CATALOGUE-----------------------------------------------------");
			for(Produit p : listProduits)
			{
		       System.out.println("----------------------------------Produit N�"+p.getIdProduit()+" :");
		       System.out.println("Nom :" + p.getNomProduit());
		       System.out.println("Categorie : " + p.getCategorie());
		       System.out.println("Espece : " + p.getEspece());
		       System.out.println("Prix unitaire : " + p.getPrixU() +" Euros ");
		       System.out.println("Quantite disponible :"+p.getQteDispo());  
		       System.out.println("---------------------------------------------------");
			}
			
			System.out.println("Saisissez le numero d'ID du produit qui vous interesse du catalogue ci-dessus:\n\n");
			int choixIdProduit = sc.nextInt();
			
			while(ProduitDAO.getProduit(choixIdProduit)==null)
			{
				System.out.println("Ce produit malheureusement n'existe pas !");
				do
				{
					System.out.println("Vous voulez continuez ? (o/n)");
					choix =  sc.next();
					if(!choix.equals("o") && !choix.equals("n"))
						System.out.println("S'il vous plait , tapez soit o pour oui ou n pour non");

				}while(!choix.equals("o") && !choix.equals("n"));
				if(choix.equals("n") && utilisateur.getPanier(nbrTransactionClient)==null) { quit=true; break;}
				else if(choix.equals("n") && utilisateur.getPanier(nbrTransactionClient).size() ==0 ) { quit=true; break;}
				else if (choix.equals("n") && utilisateur.getPanier(nbrTransactionClient).size()>0 ){commande=true; break;}
				else System.out.println("Tapez un produit existant s'il vous plait ");
				choixIdProduit = sc.nextInt();
			}
			if(!quit && choix.equals("o") )
			{
				Produit choixProduit = ProduitDAO.getProduit(choixIdProduit);
				
				System.out.println("Saisissez la quantite que vous voulez de ce produit ?");
					int choixQte = sc.nextInt();
					if(utilisateur.AjouterPanierAchat(nbrTransactionClient, choixProduit, choixQte))
					{
					   produitController.UpdateQteProduit(choixProduit.getIdProduit(),choixQte);
					   achat.AddQteDemand(choixQte);
					   acheter=true;
					   nbrAchatsClient++;
					}
					else
					{
						while(!utilisateur.AjouterPanierAchat(nbrTransactionClient, choixProduit,choixQte))
						{
							System.out.println("Quantite demandee est hors capacite, essayez encore une fois");
							do
							{
								System.out.println("Vous voulez continuez ? (o/n)");
								choix =  sc.next(); 
								if(!choix.equals("o") && !choix.equals("n"))
									System.out.println("S'il vous plait , tapez soit o pour oui ou n pour non");

							}while(!choix.equals("o") && !choix.equals("n"));
							if(choix.equals("n")) { quit=true; break;}
							System.out.println("Choisissez une quantite disponible s'il vous plait ");
							choixQte = sc.nextInt();
							if(utilisateur.AjouterPanierAchat(nbrTransactionClient, choixProduit, choixQte))
							{
								produitController.UpdateQteProduit(choixProduit.getIdProduit(),choixQte);
								achat.AddQteDemand(choixQte);
								acheter=true;
								nbrAchatsClient++;
							}
						}
					}
					do 
					{
						System.out.println("Vous voulez ajouter un autre produit ? (o/n)");
						choix =  sc.next();
						if(!choix.equals("o") && !choix.equals("n"))
							System.out.println("S'il vous plait, tapez soit o pour oui ou n pour non");
						if(choix.equals("o")) repeat=true;
						else if(choix.equals("n")) { commande=true; quit=true; break;}
					}while(!choix.equals("o") && !choix.equals("n"));
					
			}while(choix.equals("o") && !repeat);
				
		}while(repeat);
		do
		{
			System.out.println("Vous voulez remplir un autre panier ? (o/n)");
			choix = sc.next();
			if(!choix.equals("o") && !choix.equals("n"))
			{
				otherpanier=false;
				System.out.println("S'il vous plait, tapez soit o pour oui ou n pour non");
			}
			if(choix.equals("o")) {
				quit=false; otherpanier=true;
			}
			else if(choix.equals("n")) { otherpanier=true; quit=true; break;}
			
		}while(!otherpanier);
		}while(!quit);
		  
		if(nbrAchatsClient>0)
		{
			for(int i=0;i<utilisateur.getListeAchats().size(); i++)
			{
				for(Produit p : utilisateur.getAchat(i).getPanier())
				{
					p.setAchat(utilisateur.getAchat(i));
				}
			}
			System.out.println("FIN DE SAISIE\n\n\nVOTRE COMMANDE\n");
			System.out.println("Vous avez remplissez "+nbrAchatsClient+" paniers");
			System.out.println("-------------------");
			System.out.println("Entrez votre nom :");
			String nom = sc.next();
			System.out.println("Entrez votre prenom :");
			String pnom = sc.next();
			System.out.println("Entrez votre ville :");
			String ville = sc.next();
			System.out.println("Entrez votre adresse :");
			String adresse = sc.next();
			
			do
			{
				
			System.out.println("Vous avez une carte de fidelite (Reduction de 10% pour chaque produit) ? (o/n)");
			choix = sc.next();
			
			if(!choix.equals("o") && !choix.equals("n"))
			{
				quit=false;
				System.out.println("S'il vous plait, tapez soit o pour oui ou n pour non");
			}
			else{ quit=true; break;}
			
			}while(!quit);
			
			if(choix.equals("o"))
			{
				System.out.println("Vous allez beneficier d'une reduction de 10% pour chaque produit de vos paniers");
				utilisateur.setReductionEligible(true);
				utilisateur.setTauxReduction(0.1);
			}
			else 
			{
				System.out.println("Vous n'allez pas beneficier de la reduction de 10% pour vos produits");
				utilisateur.setReductionEligible(false);
				utilisateur.setTauxReduction(0);
			}

		
			utilisateur.setNomUtilisateur(nom);
			utilisateur.setPnomUtilisateur(pnom);
			utilisateur.setVilleUtilisateur(ville);
			utilisateur.setAdrUtilisateur(adresse);			
			
		    System.out.println("\nVOTRE FACTURE: \n");
		    
		    float montantTotal =0;
		    
		    for(int k=0; k<utilisateur.getListeAchats().size() ; k++)
		    {
			achat = utilisateur.getAchat(k);
			achat.CalculPrixTotal(k,utilisateur);
			if(achat.getMontantTotal()==0)
				utilisateur.getListeAchats().remove(k);
	    	}
	    
		    for(int k=0; k<utilisateur.getListeAchats().size() ; k++)
		    {
			  System.out.println("Panier N� "+(k+1)+"-------------------------------------\n");
			for (int i=0; i< utilisateur.getAchat(k).getPanier().size();i++) {
								
			    Produit prod = utilisateur.getAchat(k).getPanier().get(i);
			    int qteDemandee = utilisateur.getAchat(k).getListeQuantitesDemand().get(i);
			    
			    System.out.println("--------PRODUIT ID : "+prod.getIdProduit());
			    System.out.println("Nom : "+prod.getNomProduit());
	            System.out.println("Libelle :" + prod.getNomProduit());
	            System.out.println("Categorie : "+prod.getCategorie());
	            System.out.println("Espece : " +prod.getEspece());
	            System.out.println("Prix unitaire : " + prod.getPrixU()+" Euros");
	            if(utilisateur.isReductionEligible())
		        System.out.println("Prix unitaire apr�s r�duction de 10% : " + (prod.getPrixU()-utilisateur.getTauxReduction())+" Euros");
			    System.out.println("Quantite demandee :"+qteDemandee);
	            System.out.println("Prix total (sans TVA) : " + prod.getPrixU()*qteDemandee+" Euros");
			    if(prod.getQteDispo()>qteDemandee)
			    System.out.println("Quantite restante dans le stock :"+(prod.getQteDispo()-qteDemandee));
			    else System.out.println("Quantite restante dans le stock :"+0);
			    System.out.println("--------------------------------------------");
			  }   
			achat = utilisateur.getAchat(k);
			achat.CalculPrixTotal(k,utilisateur);
			montantTotal += achat.getMontantTotal();
			System.out.println("-->Montant du panier "+(k+1)+" + TVA : "+achat.getMontantTotal()+" Euros\n");
	    	}
			System.out.println("--------->Montant total de votre achat + TVA : "+montantTotal+" Euros\n");
			System.out.println("\n\n----------- Merci pour votre confiance --- Bonne reception.");
			ClientsDAO.addClient(utilisateur);	
		}
		System.out.println("Au revoir !");		
		EntityClass.close();	
	}

	
}
