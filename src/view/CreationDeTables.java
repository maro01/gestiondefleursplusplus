package view;

import bddutils.EntityClass;
import controleur.AchatDAO;
import controleur.ClientsDAO;
import controleur.FournisseurDAO;
import controleur.ProduitDAO;
import modele.Fournisseur;
import modele.Produit;

public class CreationDeTables {

	public static void main(String[] args) {

		
		EntityClass.init();

		FournisseurDAO fournisseurController = new FournisseurDAO();
		ProduitDAO produitController = new ProduitDAO();
		ClientsDAO clientController = new ClientsDAO();
		AchatDAO achatController = new AchatDAO();

		Fournisseur fournisseur1 = new Fournisseur();
		fournisseur1.setNomFournisseur("Nadal");
		fournisseur1.setPnomFournisseur("Bill");
		fournisseur1.setAdrFournisseur("Rue 10 Silicon Valley");
		fournisseur1.setVilleFournisseur("San Fransissco");
		
		Fournisseur fournisseur2 = new Fournisseur();
		fournisseur2.setNomFournisseur("Feley");
		fournisseur2.setPnomFournisseur("Steve");
		fournisseur2.setAdrFournisseur("02 Rue de jean-baptiste");
		fournisseur2.setVilleFournisseur("Paris");
		
		fournisseurController.addFournisseur(fournisseur1);
		fournisseurController.addFournisseur(fournisseur2);
		
		
		Produit produit1 = new Produit();
		produit1.setNomProduit("Alysse");
		produit1.setCategorie("Plante");
		produit1.setEspece("Plante rustique");
		produit1.setPrixU(50);
		produit1.setQteDispo(20);	
		produit1.setFournisseur(fournisseur1);
		
		Produit produit2 = new Produit();
		produit2.setNomProduit("Anemone");
		produit2.setCategorie("Fleur");
		produit2.setEspece("Rose");
		produit2.setPrixU(66);
		produit2.setQteDispo(60);	
		produit2.setFournisseur(fournisseur2);
		
		Produit produit3 = new Produit();
		produit3.setNomProduit("Anemone de pavot");
		produit3.setCategorie("Fleur");
		produit3.setEspece("Rose");
		produit3.setPrixU(66);
		produit3.setQteDispo(60);
		produit3.setFournisseur(fournisseur2);
		
		Produit produit4 = new Produit();
		produit4.setNomProduit("Amsonia");
		produit4.setCategorie("Fleur");
		produit4.setEspece("fleurs bleus");
		produit4.setPrixU(66);
		produit4.setQteDispo(60);
		produit4.setFournisseur(fournisseur1);
		
		Produit produit5 = new Produit();
		produit5.setNomProduit("Aneth");
		produit5.setCategorie("Fleur");
		produit5.setEspece("Herbe");
		produit5.setPrixU(66);
		produit5.setQteDispo(60);
		produit5.setFournisseur(fournisseur1);
		
		
		produitController.addProduit(produit1);
		produitController.addProduit(produit2);
		produitController.addProduit(produit3);
		produitController.addProduit(produit4);
		produitController.addProduit(produit5);
		
	}

}
