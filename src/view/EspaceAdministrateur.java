package view;

import java.util.Scanner;

import bddutils.EntityClass;
import controleur.AchatDAO;
import controleur.AdministrateurDAO;
import controleur.CommandeDAO;
import controleur.FournisseurDAO;
import controleur.ProduitDAO;
import modele.Administrateur;
import modele.Commande;
import modele.Fournisseur;
import modele.Produit;

public class EspaceAdministrateur {

	public static void main(String[] args) {
		
		EntityClass.init();
		
		int nbrTransaction = 0;

		FournisseurDAO fournisseurController = new FournisseurDAO();
		ProduitDAO produitController = new ProduitDAO();
		CommandeDAO cmdController = new CommandeDAO();
		AdministrateurDAO adminController = new AdministrateurDAO();

		
		 Scanner sc = new Scanner(System.in);
		 
			String choix="o";
			
			Administrateur admin = new Administrateur();
		
			Commande commande;

			boolean repeat,quit,cmd,acheter;
			do
			{
				commande = new Commande();
			do
			{
				repeat=false; //boolean faux si on ne veux plus ajouter de produits
				quit=false; // boolean faux tant que ne quitte pas l'application
				cmd=false; // boolean faux si on n'a pas encore remplit la liste des produits et leurs quantites
				acheter=false; //boolean faux si la transaction n'est pas realisee
		
		System.out.println("---------CATALOGUE DES FOURNISSEURS----------");
		
		for(Fournisseur f : fournisseurController.getAllFournisseurs())
		{
		    System.out.println("--------FOURNISSEUR ID : "+f.getIdFournisseur());
		    System.out.println("Nom : "+f.getNomFournisseur());
            System.out.println("Prenom :" + f.getPnomFournisseur());
            System.out.println("Adresse : "+f.getAdrFournisseur());
            System.out.println("Ville : " +f.getVilleFournisseur()+"\n");
		}
		
		System.out.println("Saisissez le numero d'ID du fournisseur qui vous interesse du catalogue ci-dessus:\n\n");
		int choixIdFournisseur = sc.nextInt();
		
		while(FournisseurDAO.getFournisseur(choixIdFournisseur)==null)
		{
			System.out.println("Ce fournisseur malheureusement n'existe pas !");
			do
			{
				System.out.println("Vous voulez continuez ? (o/n)");
				choix =  sc.next();
				if(!choix.equals("o") && !choix.equals("n"))
					System.out.println("S'il vous plait , tapez soit o pour oui ou n pour non");

			}while(!choix.equals("o") && !choix.equals("n"));
			if(choix.equals("n") && admin.getListeCommandes()==null) { quit=true; break;}
			else if(choix.equals("n") && admin.getListeCommandes().size() ==0 ) { quit=true; break;}
			else if (choix.equals("n") && admin.getListeCommandes().size()>0 ){cmd=true; break;}
			else System.out.println("Tapez un Id de fournisseur existant s'il vous plait ");
			choixIdFournisseur = sc.nextInt();
		}
		if(!quit && choix.equals("o") )
		{   
			Fournisseur choixFournisseur = FournisseurDAO.getFournisseur(choixIdFournisseur);
			
			System.out.println("---------CATALOGUE DES PRODUITS DU FOURNISSEUR ID = "+choixIdFournisseur+"----------");
			
			for(Produit p : choixFournisseur.getProduits())
			{
		       System.out.println("----------------------------------Produit N�"+p.getIdProduit()+" :");
		       System.out.println("Nom :" + p.getNomProduit());
		       System.out.println("Categorie : " + p.getCategorie());
		       System.out.println("Espece : " + p.getEspece());
		       System.out.println("---------------------------------------------------");
			}
			Produit choixProduit=null;
			System.out.println("Saisissez le numero d'ID du produit qui vous interesse du catalogue ci-dessus:\n\n");
			int choixIdProduit = sc.nextInt();
			int choixQte=0;
			while(ProduitDAO.getProduit(choixIdProduit,choixIdFournisseur)==null)
			{
				System.out.println("Ce produit malheureusement n'existe pas !");
				do
				{
					System.out.println("Vous voulez continuez ? (o/n)");
					choix =  sc.next();
					if(!choix.equals("o") && !choix.equals("n"))
						System.out.println("S'il vous plait , tapez soit o pour oui ou n pour non");

				}while(!choix.equals("o") && !choix.equals("n"));
				if(choix.equals("n") && admin.getListeCommandes()==null) { quit=true; break;}
				else if(choix.equals("n") && admin.getListeCommandes().size() ==0 ) { quit=true; break;}
				else if (choix.equals("n") && admin.getListeCommandes().size()>0 ){cmd=true; break;}
				else System.out.println("Tapez un produit existant s'il vous plait ");
				choixIdProduit = sc.nextInt();
			}
			if(!quit && choix.equals("o") )
			{
				choixProduit = ProduitDAO.getProduit(choixIdProduit);
				
				do
				{
				  System.out.println("Saisissez la quantite que vous voulez de ce produit ?");
					choixQte = sc.nextInt();
					if(choixQte<=0) 
					{
						System.out.println("Merci de saisir une quantite valide s'il vous plait");
						do
						{
							System.out.println("Vous voulez continuez ? (o/n)");
							choix =  sc.next();
							if(!choix.equals("o") && !choix.equals("n"))
								System.out.println("S'il vous plait , tapez soit o pour oui ou n pour non");

						}while(!choix.equals("o") && !choix.equals("n"));
						if(choix.equals("n") && admin.getListeCommandes()==null) { quit=true; break;}
						else if(choix.equals("n") && admin.getListeCommandes().size() ==0 ) { quit=true; break;}
						else if (choix.equals("n") && admin.getListeCommandes().size()>0 ){cmd=true; break;}
						else System.out.println("Tapez une quantite valide s'il vous plait ");
						choixQte = sc.nextInt();
					}}while(choixQte<=0);
				        
					}
					if(choixProduit!=null && choixQte>0)
					{
						commande.AjouterProduit(choixProduit);
						commande.AjouterQteDemandee(choixQte);
						commande.setAdmin(admin);
						admin.AjouterCommande(commande);
						produitController.AjoutStockProduit(choixIdProduit, choixQte);
					    acheter=true;
					}

					do 
					{
						System.out.println("Vous voulez ajouter un autre produit ? (o/n)");
						choix =  sc.next();
						if(!choix.equals("o") && !choix.equals("n"))
							System.out.println("S'il vous plait, tapez soit o pour oui ou n pour non");
						if(choix.equals("o")) repeat=true;
						else if(choix.equals("n")) { cmd=true; quit=true; break;}
						
					}while(!choix.equals("o") && !choix.equals("n"));
					
			}while(choix.equals("o") && !repeat);
		    }while(repeat);
			
			}while(!quit && choix.equals("o"));
			
			if(acheter==true)
			{
				for(Produit p : commande.getListProduits())
					p.setCommande(commande);
				
				System.out.println("FIN DE SAISIE\n\n\nVOTRE COMMANDE");
				System.out.println("-------------------");
				admin.setNomUtilisateur("admin");
				admin.setPnomUtilisateur("admin");
			    
				for(Commande c : admin.getListeCommandes())
				{   int i=0;
					for(Produit p : c.getListProduits())
					{     
					       System.out.println("----------------------------------Produit N�"+p.getIdProduit()+" :");
					       System.out.println("Nom :" + p.getNomProduit());
					       System.out.println("Categorie : " + p.getCategorie());
					       System.out.println("Espece : " + p.getEspece());
					       System.out.println("Qte demandee : "+c.getListQuantitesDemand().get(i));
					       System.out.println("Fournisseur : "+p.getFournisseur().getNomFournisseur()+" "+p.getFournisseur().getPnomFournisseur());
					       System.out.println("---------------------------------------------------");
					       i++;
					}
				}
				System.out.println("-------La livraison sera faite dans les meilleurs delais. Bien a vous.\n\n");
				
				adminController.addAdministrateur(admin);
			
            }
			System.out.println("Au revoir !");
			}
}

