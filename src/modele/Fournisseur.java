package modele;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
public class Fournisseur {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idFournisseur;
	private String nomFournisseur;
	private String pnomFournisseur;
	private String adrFournisseur;
	private String villeFournisseur;
	
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "fournisseur")
	private List<Produit> produits;
	
	public Fournisseur() {
		this.nomFournisseur = null;
		this.pnomFournisseur = null;
		this.adrFournisseur = null;
		this.villeFournisseur = null;
		this.produits = null;
	}
	
	public Fournisseur(String nomFournisseur, String pnomFournisseur, String adrFournisseur,
			String villeFournisseur, List<Produit> produits) {
		this.nomFournisseur = nomFournisseur;
		this.pnomFournisseur = pnomFournisseur;
		this.adrFournisseur = adrFournisseur;
		this.villeFournisseur = villeFournisseur;
		this.produits = produits;
	}
	
	

	public int getIdFournisseur() {
		return idFournisseur;
	}
	
	public String getNomFournisseur() {
		return nomFournisseur;
	}

	public void setNomFournisseur(String nomFournisseur) {
		this.nomFournisseur = nomFournisseur;
	}

	public String getPnomFournisseur() {
		return pnomFournisseur;
	}

	public void setPnomFournisseur(String pnomFournisseur) {
		this.pnomFournisseur = pnomFournisseur;
	}

	public String getAdrFournisseur() {
		return adrFournisseur;
	}

	public void setAdrFournisseur(String adrFournisseur) {
		this.adrFournisseur = adrFournisseur;
	}

	public String getVilleFournisseur() {
		return villeFournisseur;
	}

	public void setVilleFournisseur(String villeFournisseur) {
		this.villeFournisseur = villeFournisseur;
	}

	public void AddProduit(Produit produit) {
		produits.add(produit);
	}

	public List<Produit> getProduits() {
		return produits;
	}

	public void setProduits(List<Produit> produits) {
		this.produits = produits;
	}
}
