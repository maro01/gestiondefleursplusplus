package modele;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.*;


@Entity
public class Achat {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idAchat;

	private float montantTotal;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "achat")
	private List<Produit> panier;
	
	@Column
    @ElementCollection(targetClass=Integer.class)
	private List<Integer> listeQuantitesDemand; 
	
    @ManyToOne
	private Client client;

	
    
	public int getIdAchat() {
		return idAchat;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Achat() {
		panier = new ArrayList<>();
		listeQuantitesDemand = new ArrayList<>();
	}
	
	public Achat(List<Produit> panier, List<Integer> listQte) {
		this.panier = panier;
		this.listeQuantitesDemand = listQte;
	}

	public List<Produit> getPanier() {
		return panier;
	}

	public void setPanier(List<Produit> panier) {
		this.panier = panier;
	}
	

	public List<Integer> getListeQuantitesDemand() {
		return listeQuantitesDemand;
	}

	public void setListeQuantitesDemand(List<Integer> listeQuantitesDemand) {
		this.listeQuantitesDemand = listeQuantitesDemand;
	}
	
	public void AddQteDemand(int Qte) {
		this.listeQuantitesDemand.add(Qte);
	}

	public float getMontantTotal() {
		return montantTotal;
	}

	public void setMontantTotal(float montantTotal) {
		this.montantTotal = montantTotal;
	}
	
	public void CalculPrixTotal(int index,Client utilisateur)
	{
	    Achat achat = utilisateur.getAchat(index);
		float prix = 0;
		
		for (int i=0; i<achat.getPanier().size(); i++) {
			
		    Produit prod = panier.get(i);
		    int qteDemandee = listeQuantitesDemand.get(i);		    
			prix += (prod.getPrixU() - utilisateur.getTauxReduction() ) * 1.15 * qteDemandee; 
		}
		
		 montantTotal=prix;
	}
		
}
