package modele;

import javax.persistence.*;


@Entity
public class Produit {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idProduit;
	private String nomProduit;
	private String categorie;
	private String espece;
	private int qteDispo;
	private float prixU;
	
	@ManyToOne
	private Fournisseur fournisseur;

	@ManyToOne
	private Achat achat;
	
	@ManyToOne
	private Commande commande;
	
	public Produit() {
		this.nomProduit =  null;
		this.categorie = null;
		this.espece = null;
		this.qteDispo = 0;
		this.prixU = 0;
		this.fournisseur = null;
	}
	
	public Produit(String nomProduit, String categorie, String espece, int qte, float prixU,Fournisseur fournisseur) {
		this.nomProduit = nomProduit;
		this.categorie = categorie;
		this.espece = espece;
		this.qteDispo = qte;
		this.prixU = prixU;
		this.fournisseur=fournisseur;
	}
	
	
	public Commande getCommande() {
		return commande;
	}

	public void setCommande(Commande commande) {
		this.commande = commande;
	}

	
	
	public int getIdProduit() {
		return idProduit;
	}


	public int getQteDispo() {
		return qteDispo;
	}

	public void setQteDispo(int qteDispo) {
		this.qteDispo = qteDispo;
	}

	public Achat getAchat() {
		return achat;
	}

	public void setAchat(Achat achat) {
		this.achat = achat;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public String getNomProduit() {
		return nomProduit;
	}

	public void setNomProduit(String nomProduit) {
		this.nomProduit = nomProduit;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public String getEspece() {
		return espece;
	}

	public void setEspece(String espece) {
		this.espece = espece;
	}


	public float getPrixU() {
		return prixU;
	}

	public void setPrixU(float prixU) {
		this.prixU = prixU;
	}
	
	
	
}
