package modele;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.MapKeyColumn;
import javax.persistence.MapKeyEnumerated;
import javax.persistence.OneToMany;

@Entity
public class Commande {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idCommande;
	
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy= "commande")
	private List<Produit> listProduits;
	
	@Column
    @ElementCollection(targetClass=Integer.class)
	private List<Integer> listQuantitesDemand;
	
	@ManyToOne
	private Administrateur admin;
	

	public Commande() {
		this.listProduits = new ArrayList<>();
		this.listQuantitesDemand = new ArrayList<>();
	}
	
	public Commande(List<Produit> listProduits) {
		this.listProduits = listProduits;
	}

	
	
	public int getIdCommande() {
		return idCommande;
	}

	public List<Produit> getListProduits() {
		return listProduits;
	}
	

	public void setListProduits(List<Produit> listProduits) {
		this.listProduits = listProduits;
	}

	public List<Integer> getListQuantitesDemand() {
		return listQuantitesDemand;
	}

	public void setListQuantitesDemand(List<Integer> listQuantitesDemand) {
		this.listQuantitesDemand = listQuantitesDemand;
	}

	public Administrateur getAdmin() {
		return admin;
	}

	public void setAdmin(Administrateur admin) {
		this.admin = admin;
	}
	
	public boolean AjouterProduit(Produit produit) {
		if(produit!=null)
		{
			listProduits.add(produit);
			return true;
		}
		return false;
	}
	public boolean AjouterQteDemandee(int QteDemandee) {
		if(QteDemandee>0)
		{
			listQuantitesDemand.add(QteDemandee);
			return true;
		}
		return false;
	}
	

}
