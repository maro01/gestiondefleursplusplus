package modele;

import javax.persistence.*;


@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE")
public class Utilisateur {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idUtilisateur;
	private String nomUtilisateur;
	private String pnomUtilisateur;
	private String adrUtilisateur;
	private String villeUtilisateur;
	

	public Utilisateur() {
		this.nomUtilisateur = null;
		this.pnomUtilisateur = null;
		this.adrUtilisateur = null;
		this.villeUtilisateur = null;
	}
	
	public Utilisateur(String nomClient, String pnomClient, String adrClient, String villeClient) 
	{
		this.nomUtilisateur = nomClient;
		this.pnomUtilisateur = pnomClient;
		this.adrUtilisateur = adrClient;
		this.villeUtilisateur = villeClient;
	}

	public int getIdUtilisateur() {
		return idUtilisateur;
	}

	public String getNomUtilisateur() {
		return nomUtilisateur;
	}

	public void setNomUtilisateur(String nomUtilisateur) {
		this.nomUtilisateur = nomUtilisateur;
	}

	public String getPnomUtilisateur() {
		return pnomUtilisateur;
	}

	public void setPnomUtilisateur(String pnomUtilisateur) {
		this.pnomUtilisateur = pnomUtilisateur;
	}

	public String getAdrUtilisateur() {
		return adrUtilisateur;
	}

	public void setAdrUtilisateur(String adrUtilisateur) {
		this.adrUtilisateur = adrUtilisateur;
	}

	public String getVilleUtilisateur() {
		return villeUtilisateur;
	}

	public void setVilleUtilisateur(String villeUtilisateur) {
		this.villeUtilisateur = villeUtilisateur;
	}

}
