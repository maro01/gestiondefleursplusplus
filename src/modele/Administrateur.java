package modele;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;


@Entity
@DiscriminatorValue (value = "ADM")
public class Administrateur extends Utilisateur {
	
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "admin")
	private List<Commande> listeCommandes;
	
	
	public Administrateur()
	{
		super();
		listeCommandes = new ArrayList<>();
	}

	public List<Commande> getListeCommandes() {
		return listeCommandes;
	}

	public void setListeCommandes(List<Commande> listeCommandes) {
		this.listeCommandes = listeCommandes;
	}
	
	
	public boolean AjouterCommande(Commande commande) {
		if(commande!=null)
		{
			listeCommandes.add(commande);
			return true;
		}
		else return false;
	}
	
	
}
