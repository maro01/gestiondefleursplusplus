package modele;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.*;


@Entity
@DiscriminatorValue (value = "CL")
public class Client extends Utilisateur{
	
	private boolean reductionEligible;
	
	private double tauxReduction;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
	private List<Achat> listeAchats;
	
	public Client()
	{
		super();
		reductionEligible=false;
		listeAchats = new ArrayList<Achat>();
	}


	public boolean isReductionEligible() {
		return reductionEligible;
	}

	public void setReductionEligible(boolean reductionEligible) {
		this.reductionEligible = reductionEligible;
		
		if(!reductionEligible) tauxReduction=0;
	}

	public double getTauxReduction() {
		return tauxReduction;
	}

	public void setTauxReduction(double tauxReduction) {
		this.tauxReduction = tauxReduction;
	}

	public List<Achat> getListeAchats() {
		return listeAchats;
	}
	
	public Achat getAchat(int index) {
		return listeAchats.get(index);
	}
	
	public List<Produit> getPanier(int index) {
		if(listeAchats.size()>0 && index<=listeAchats.size())
		return listeAchats.get(index).getPanier();
		
		else return null;
	}
	
	public void AjouterPanier(Achat achat) {
		listeAchats.add(achat);
	}
	
	public boolean AjouterPanierAchat(int nbrTransaction ,Produit p,int Qte) {
		if(p!=null && ( Qte > 0 && Qte <= p.getQteDispo() ) )
		{
			listeAchats.get(nbrTransaction).AddQteDemand(Qte);
			listeAchats.get(nbrTransaction).getPanier().add(p);
			return true;
		}
		return false;
	}
	

	public void setListeAchats(List<Achat> listeAchats) {
		this.listeAchats = listeAchats;
	}
	
	
}
